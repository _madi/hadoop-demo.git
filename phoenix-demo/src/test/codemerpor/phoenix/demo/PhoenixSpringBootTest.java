package codemerpor.phoenix.demo;

import codemerpor.phoenix.demo.entity.Demo;
import codemerpor.phoenix.demo.mapper.DemoMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
@Slf4j
public class PhoenixSpringBootTest {
    @Autowired
    private DemoMapper demoMapper;

    @Test
    public void test() {
        List<Demo> demoList = demoMapper.selectAll();
        System.out.println(demoList.toString());
    }
}
