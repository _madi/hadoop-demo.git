package codemerpor.phoenix.demo;

import java.sql.*;

public class PhoenixDemo {
    public static void main(String[] args) throws Exception {
        Class.forName("org.apache.phoenix.jdbc.PhoenixDriver");

        //这里博主的电脑配置了hosts：master、node1、node2
        Connection connection = DriverManager.getConnection("jdbc:phoenix:master:2181");
        PreparedStatement preparedStatement = connection.prepareStatement("select * from demo");
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            System.out.println("id:" + resultSet.getString("id") + "========" +
                    "name:" + resultSet.getString("name") + "========" +
                    "sex:" + resultSet.getString("sex")
            );
        }

        preparedStatement.close();
        connection.close();
    }
}
