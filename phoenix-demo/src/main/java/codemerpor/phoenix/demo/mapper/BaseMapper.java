package codemerpor.phoenix.demo.mapper;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * @author codemperor
 */
public interface BaseMapper<T> extends Mapper<T>, MySqlMapper<T> {
}
