package codemerpor.phoenix.demo.mapper;

import codemerpor.phoenix.demo.entity.Demo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * @author codemperor
 */
@Component
@Mapper
public interface DemoMapper extends BaseMapper<Demo> {
}
