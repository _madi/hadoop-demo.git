package codemperor.mapreduce.wc;

import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

/**
 * 执行map和reduce
 */
@Slf4j
public class WordCountDriver {
    public static void main(String[] args) throws Exception {
        String HDFSURI = "hdfs://master:9000";
        /**********配置********/
        Configuration configuration = new Configuration();
        //这里针对外网链接测试，设置datanode的hostname通信
        configuration.set("dfs.client.use.datanode.hostname", "true");

        log.info("go go go");

        //编写作业
        Job job = Job.getInstance(configuration, "WordCount");
        job.setJarByClass(WordCountDriver.class);
        job.setMapperClass(WordCountMapper.class);
        job.setReducerClass(WordCountReduce.class);

        //设置map的输出类型
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IntWritable.class);

        //设置reduce的输出类型
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        //作业输入和输出的参数
//        FileInputFormat.setInputPaths(job, new Path(HDFSURI + "/hdfsapi/wordcount"));
//        FileOutputFormat.setOutputPath(job, new Path(HDFSURI + "/wordcount/output"));

        //下面是可以本地执行mr计算本地文件
        FileInputFormat.setInputPaths(job, new Path("hdfs-demo/input/wordcount"));
        FileOutputFormat.setOutputPath(job, new Path("hdfs-demo/output/result"));

        boolean result = job.waitForCompletion(true);
        System.out.println(result);
    }
}
