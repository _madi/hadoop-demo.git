package codemperor.mapreduce.wc;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * 使用MR来实现wordcount
 * KEYIN: 每行数据的偏移量,比如第一行数据共20个字符，那么第一行偏移量是0，第二行偏移量是20
 * VALUEIN：正行数据
 * KEYOUT: 数据输出的key，比如词频统计，输出的key就是某个单词
 * VALUEOUT：输出的value
 * <p>
 * Mapper后面4个类型不要跟java的类型，而是跟hadoop自带的可序列化以及反序列化的基础类型
 */
public class WordCountMapper extends Mapper<LongWritable, Text, Text, IntWritable> {

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String[] values = value.toString().split(" ");
        for (String word : values) {
            context.write(new Text(word), new IntWritable(1));
        }
    }
}
