package codemperor.mapreduce.accesslog.dailycount;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Partitioner;

/**
 * 指定map之后数据key的切分规则，Partitioner参数是map的输出
 */
public class DailyCountPartitioner extends Partitioner<Text, DailyCountEntity> {

    /**
     * @param text
     * @param dailyCountEntity
     * @param numPartitions    作业的指定的reducer个数，决定了reduce输出文件的个数
     * @return
     */
    @Override
    public int getPartition(Text text, DailyCountEntity dailyCountEntity, int numPartitions) {

        if (dailyCountEntity.getFailed() == 1) {
            //失败的一个文件
            return 0;
        } else if (dailyCountEntity.getSuccess() == 1) {
            //成功的一个文件
            return 1;
        } else {
            //剩下的全部放在另一个文件中
            return 2;
        }
    }
}
