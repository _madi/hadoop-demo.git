package codemperor.mapreduce.accesslog.dailycount;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.log4j.Logger;

/**
 * 执行map和reduce
 */
public class DailyCountDriver {
    public static Logger log  = Logger.getLogger(DailyCountDriver.class);

    public static void main(String[] args) throws Exception {
        /**********配置********/
        Configuration configuration = new Configuration();
        log.info("daily count go go go");

        //编写作业
        Job job = Job.getInstance(configuration, "daily count");
        job.setJarByClass(DailyCountDriver.class);
        job.setMapperClass(DailyCountMapper.class);
        job.setReducerClass(DailyCountReduce.class);

        //设置分区和分区数量
        job.setPartitionerClass(DailyCountPartitioner.class);
        job.setNumReduceTasks(3);

        //设置map的输出类型
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(DailyCountEntity.class);

        //设置reduce的输出类型
        job.setOutputKeyClass(NullWritable.class);
        job.setOutputValueClass(DailyCountEntity.class);

        //下面是可以本地执行mr计算本地文件
        FileInputFormat.setInputPaths(job, new Path("hdfs-demo/src/main/java/codemperor/mapreduce/accesslog/access_2013_05_31.log"));
        FileOutputFormat.setOutputPath(job, new Path("hdfs-demo/output/daily_count_result"));

        //打成jar之后，可以使用下面代码，使用hadoop jar xx.jar codemperor.mapreduce.accesslog.dailycount.DailyCountDriver /data/input/access_2013_05_31.log /output/daily_count_result
//        FileInputFormat.setInputPaths(job, new Path(args[0]));
//        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        boolean result = job.waitForCompletion(true);
        System.out.println(result);
    }
}
