package codemperor.mapreduce.accesslog.dailycount;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class DailyCountMapper extends Mapper<LongWritable, Text, Text, DailyCountEntity> {
    public Logger log = Logger.getLogger(DailyCountDriver.class);

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        //27.19.74.143 - - [30/May/2013:17:38:20 +0800] "GET /static/image/common/faq.gif HTTP/1.1" 200 1127
        String val = value.toString();
        log.info("[map] value: " + val);
        String reduceKey = dateFormat(val);
        String methodCode = getResult(val).trim();

        DailyCountEntity dailyCountEntity = new DailyCountEntity();
        if ("0".equals(methodCode)) {
            dailyCountEntity.setUndefined(1);
        } else if ("200".equals(methodCode)) {
            dailyCountEntity.setSuccess(1);
        } else {
            dailyCountEntity.setFailed(1);
        }

        context.write(new Text(reduceKey), dailyCountEntity);
    }

    public String dateFormat(String val) {
        String date = val.substring(val.indexOf("[") + 1, val.indexOf("]"));
        SimpleDateFormat in = new SimpleDateFormat("dd/MMM/yyyy:HH:mm:ss ZZZZZ", Locale.US);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:00:00");
        try {
            String result = sdf.format(in.parse(date));
            log.info("[dateFormat] date: " + result);
            return result;
        } catch (ParseException e) {
            log.error("", e);
        }
        return val;
    }

    /**
     * 获取方法调用的code值
     *
     * @param val
     * @return
     */
    public String getResult(String val) {
        // 27.19.74.143 - - [30/May/2013:17:38:20 +0800] "GET /static/image/common/faq.gif HTTP/1.1" 200 1127
        //200前面有个空格
        try {
            val = val.substring(val.lastIndexOf("\"") + 2);
            String[] valArr = val.split(" ");
            log.info("[getResult] method code: " + valArr[0]);
            return valArr[0];
        } catch (Exception e) {
            log.error("", e);
        }
        return "0";
    }

    public static void main(String[] args) {
        String val = "27.19.74.143 - - [30/May/2013:17:38:20 +0800] \"GET /static/image/common/faq.gif HTTP/1.1\" 200 1127";
        System.out.println(new DailyCountMapper().dateFormat(val));
    }
}
