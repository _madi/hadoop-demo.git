package codemperor.mapreduce.accesslog.dailycount;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class DailyCountReduce extends Reducer<Text, DailyCountEntity, NullWritable, DailyCountEntity> {

    @Override
    protected void reduce(Text key, Iterable<DailyCountEntity> values, Context context) throws IOException, InterruptedException {

        System.out.println("begin reduce");
        int success = 0;
        int failed = 0;
        int undefined = 0;
        for (DailyCountEntity dailyCountEntity : values) {
            success += dailyCountEntity.getSuccess();
            failed += dailyCountEntity.getFailed();
            undefined += dailyCountEntity.getUndefined();
        }

        DailyCountEntity dailyCountEntity = new DailyCountEntity();
        dailyCountEntity.setFailed(failed);
        dailyCountEntity.setSuccess(success);
        dailyCountEntity.setUndefined(undefined);
        dailyCountEntity.setDate(key.toString());

        context.write(NullWritable.get(), dailyCountEntity);
    }
}
