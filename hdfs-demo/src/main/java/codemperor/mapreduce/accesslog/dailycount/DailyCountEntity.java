package codemperor.mapreduce.accesslog.dailycount;

import lombok.Getter;
import lombok.Setter;
import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * 计算每日请求成功量与失败量
 */
@Getter
@Setter
public class DailyCountEntity implements Writable {
    /**
     *
     */
    private String date = "";
    /**
     * 成功次数
     */
    private Integer success = 0;
    /**
     * 失败次数
     */
    private Integer failed = 0;

    /**
     * 解析失败
     */
    private Integer undefined = 0;

    public DailyCountEntity() {
    }

    /**
     * write和readFields执行，必须有值，因为是流的写入写出
     *
     * @param out
     * @throws IOException
     */
    @Override
    public void write(DataOutput out) throws IOException {
        out.writeUTF(date);
        out.writeInt(success);
        out.writeInt(failed);
        out.writeInt(undefined);
    }

    /**
     * 读的顺序必须和上面写的顺序一致
     *
     * @param in
     * @throws IOException
     */
    @Override
    public void readFields(DataInput in) throws IOException {
        this.date = in.readUTF();
        this.success = in.readInt();
        this.failed = in.readInt();
        this.undefined = in.readInt();
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{");
        sb.append("\"date\":\"")
                .append(date).append('\"');
        sb.append(",\"success\":")
                .append(success);
        sb.append(",\"failed\":")
                .append(failed);
        sb.append(",\"undefined\":")
                .append(undefined);
        sb.append('}');
        return sb.toString();
    }
}
