package codemperor.hbase;

import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.hadoop.hbase.HbaseTemplate;
import org.springframework.data.hadoop.hbase.RowMapper;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
@Slf4j
public class HBaseSpringbootTest {
    @Autowired
    private HbaseTemplate hbaseTemplate;

    @Test
    public void putTest() {
        hbaseTemplate.put("hbase_demo", "rowKey1", "family1", "column1", Bytes.toBytes("test for data"));
    }

    @Test
    public void getTest() {
        HBaseMapper mapper = new HBaseMapper();
        hbaseTemplate.get("hbase_demo", "rowKey1", mapper);
    }

    public class HBaseMapper implements RowMapper {

        @Override
        public Object mapRow(Result result, int i) throws Exception {
            System.out.println("rowkey=" + Bytes.toString(result.getRow()));
            System.out.println("value=" + Bytes
                    .toString(result.getValue(Bytes.toBytes("family1"), Bytes.toBytes("column1"))));

            return result;
        }
    }
}
